package edu.purdue.software_design;

import static org.junit.Assert.*;

import org.junit.Test;

public class AlleghenyTollBoothTest {

	@Test
	public void calculateTollTest() {
		TollBooth toll = new AlleghenyTollBooth();
		Truck truck1 = new NissanTruck(5,12000);
		int expectedToll = toll.calculateToll(truck1);
		int actualToll = 145; 
		assertEquals(expectedToll, actualToll);
	}
	
	@Test
	public void clearReceiptsAndTrucksTest() {
		TollBooth toll = new AlleghenyTollBooth();
		int expectedToll = toll.receiptOfCollection();
		int actualToll = 0; 
		assertEquals(expectedToll, actualToll);
	}
}

