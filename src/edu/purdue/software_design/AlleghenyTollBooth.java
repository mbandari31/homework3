package edu.purdue.software_design;
/**
 * 
 * @author Mounika
 *
 */
public class AlleghenyTollBooth extends TollBooth {
	public AlleghenyTollBooth(){

	}
	@Override
	public void displayData() {
		System.out.print("\n Totals since last collection - Receipts: $" +getReceipts());
		System.out.print(" Trucks: " +getTotalTrucks());
	}
	@Override
	public int calculateToll(Truck truck) {
		int totalTrucks = getTotalTrucks();
		int receipts = getReceipts();
		int currentTollBill = (5 * truck.getAxles()) + (10 * truck.getWeight()/1000);
		System.out.print("\n Truck arrival - axles: " +truck.getAxles());
        System.out.print(" Total weight: " +truck.getWeight());
        System.out.print(" Toll due: $" +currentTollBill);
        totalTrucks = totalTrucks + 1;
        receipts = receipts + currentTollBill;
		setReceipts(receipts);
		setTotalTrucks(totalTrucks);
		return currentTollBill;
	}
	@Override
	public int receiptOfCollection() {
		System.out.println("\n");
		System.out.println("\n ***Collecting Receipts***");
		displayData();
		clearReceiptsAndTrucks();
		return getReceipts();
	}
}
