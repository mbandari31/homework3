package edu.purdue.software_design;
/**
 * 
 * @author Mounika
 *
 */
public class DaewooTruck implements Truck {

	private int axles;
	private int weight;
	
	public DaewooTruck(int axles, int weight) {
        this.axles = axles;
        this.weight = weight;
    }
    public int getAxles() {
    	return axles;
    	}
    public int getWeight() {
    	return weight;
    	}
    public void setValues(int axles, int weight)
    {
    	this.axles = axles;
        this.weight = weight;
    }
}
