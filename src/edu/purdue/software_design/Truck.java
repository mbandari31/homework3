package edu.purdue.software_design;
/**
 * It is used as a type.
 * @author Mounika
 *
 */
public interface Truck {
	void setValues(int axles, int weight);
    int getAxles() ;
    int getWeight();
}
