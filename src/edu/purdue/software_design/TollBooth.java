package edu.purdue.software_design;
/**
 * 
 * @author Mounika
 * This class is used to track the total trucks and receipts passed through tollBooth
 */
public abstract class TollBooth {
	/**
	 * It represents the total number of trucks.
	 */
	private int totalTrucks;
	/**
	 * It represents the total receipts of the trucks. 
	 */
	private int receipts;
	
	public TollBooth()
	{
		clearReceiptsAndTrucks();
	}
	/**
	 * It displays the total trucks and receipts. 
	 */
	public abstract void displayData();
	
	/**
	 * It calculates the amount the user has to pay according to the truck axles and weight.
	 * The present trucks and receipts are added to the total trucks and receipts collection.
	 * @param truck It has the truck axles and weight data.
	 */
	public abstract int calculateToll(Truck truck); 
	
	/**
	 * It displays the total trucks count and their receipts from the tollBooth.
	 */
	public abstract int receiptOfCollection();
	
	
	public int getReceipts()
	{
		return receipts;
	}
	public int getTotalTrucks()
	{
		return totalTrucks;
	}
	public void setTotalTrucks(int totalTrucks)
	{
		 this.totalTrucks = totalTrucks;
	}
	public void setReceipts(int receipts)
	{
		 this.receipts = receipts;
	}
	/**
	 * It resets the trucks and Receipts values to zero.
	 */
	public int clearReceiptsAndTrucks()
	{
		int resetValue = 0;
		setReceipts(resetValue);
		setTotalTrucks(resetValue);
		return getReceipts();
	}
}
