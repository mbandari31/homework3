package edu.purdue.software_design;
/**
 * 
 * @author Mounika
 *
 */
public class NissanTruck implements Truck {

	private int axles;
	private int weight;
	
	public NissanTruck(int axles, int weight) {
		setValues(axles,weight);
    }
    public int getAxles() {
    	return axles;
    	}
    public int getWeight() {
    	return weight;
    	}
    
    public void setValues(int axles, int weight)
    {
    	this.axles = axles;
        this.weight = weight;
    }
}
